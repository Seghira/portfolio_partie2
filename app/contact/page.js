'use client'
import'./page.css'
import { useState } from "react";
export default function Contact() {
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    message: "",
  });
  const [errors, setErrors] = useState({});
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });

  };

 

  const handleSubmit = (e) => {
    e.preventDefault();
    const newErrors = {};
    if (!formData.firstName) {
      newErrors.firstName = "Le prénom est requis";
    }
    if (!formData.lastName) {
      newErrors.lastName = "Le nom est requis";
    }

 

    if (!formData.email) {
      newErrors.email = "L'email est requis";
    } else if (!/\S+@\S+\.\S+/.test(formData.email)) {
      newErrors.email = "L'email est invalide";
    }
    if (!formData.message) {
      newErrors.message = "Le message est requis";
    }
    setErrors(newErrors);
    if (Object.keys(newErrors).length === 0) {
      console.log(formData);
      alert("Votre formulaire a été soumis avec succès !");
      handleReset();
    }
  };

  const handleReset = () => {
    setFormData({
      firstName: "",
      lastName: "",
      email: "",
      message: "",
    });
    setErrors({});
  };
 
  return (
    <div>
      
      <h1>Contactez-nous</h1>
      <form onSubmit={handleSubmit} >
        <div>
          <label htmlFor="firstName" >
            Prénom
          </label>
          <input
            type="text"
            id="firstName"
            name="firstName"
            value={formData.firstName}
            onChange={handleChange}
           
            placeholder="Entrez votre prénom"
          />

          {errors.firstName && (

            <p>{errors.firstName}</p>

          )}

        </div>

        {/* Ajout du hint pour le prénom */}

        <div>

          <label htmlFor="lastName">

            Nom

          </label>

          <input

            type="text"

            id="lastName"

            name="lastName"

            value={formData.lastName}

            onChange={handleChange}

        
            placeholder="Entrez votre nom"

          />

          {errors.lastName && (

            <p>{errors.lastName}</p>

          )}

        </div>

        {/* Ajout du hint pour le nom */}

        <div>

          <label htmlFor="email">

            Email

          </label>

          <input

            type="email"

            id="email"

            name="email"

            value={formData.email}

            onChange={handleChange}

           

            placeholder="Entrez votre adresse email"

          />

          {errors.email && (

            <p>{errors.email}</p>

          )}

        </div>

       

        <div>

          <label htmlFor="message">

            Message

          </label>

          <textarea

            id="message"

            name="message"

            value={formData.message}

            onChange={handleChange}

            rows="4"

       

            placeholder="Entrez votre message"

          />

          {errors.message && (

            <p>{errors.message}</p>

          )}

        </div>

       

        <div >

          <button

            type="submit"

            

          >

            Envoyer

          </button>

         

          <button

            type="button"

            onClick={handleReset}

            
          >

            Réinitialiser

          </button>

        </div>

      </form>

    </div>

  );

}
