const Footer = () => {
    return (
        <div style={{ minHeight: '100vh', display: 'flex', flexDirection: 'column' }}>
            <div style={{ flex: '1' }}>
               
            </div>
            <footer style={{ textAlign: 'center', width: '100%', padding: '20px', backgroundColor: 'pink' }}>
                <ul>
                    <a href="https://gitlab.com/Seghira">Gitlab  </a>
                    <a href="https://www.linkedin.com/in/seghira-akkouche-75b2a4160/">Linkedin  </a>
                   
                </ul>
                <p>© {new Date().getFullYear()} Seghira Akkouche </p>
            </footer>
        </div>
    );
}

export default Footer;
