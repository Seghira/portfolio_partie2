import Link from "next/link";

const Header = () => {
    return (
        <nav style={{ backgroundColor: 'pink' }}>
            <ul style={{ display: 'flex', justifyContent: 'space-around', listStyle: 'none', padding: '0', margin: '0', fontFamily: 'Arial, sans-serif' }}>
                <li><Link style={{ color: 'black', fontSize: '20px', textDecoration: 'none', fontWeight: 'bold' }} href='/home'>A propos</Link></li>
                <li><Link style={{ color: 'black', fontSize: '20px', textDecoration: 'none', fontWeight: 'bold' }} href='/projets'>Projets</Link></li>
                <li><Link style={{ color: 'black', fontSize: '20px', textDecoration: 'none', fontWeight: 'bold' }} href='/contact'>Contact</Link></li>
                <li><Link style={{ color: 'black', fontSize: '20px', textDecoration: 'none', fontWeight: 'bold' }} href='/user-list'>Témoignage</Link></li>
            </ul>
        </nav>
    );
}

export default Header;
